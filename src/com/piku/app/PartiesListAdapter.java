package com.piku.app;

import java.text.SimpleDateFormat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.piku.cloud.common.model.Party;

public class PartiesListAdapter extends ArrayAdapter<Party> {

	private final Context context;
	private final Party[] values;
	
	public PartiesListAdapter(Context context, Party[] values) {
		super(context, R.layout.listview, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.listview, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.rowTextView);
//		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
		String dateStr = sdf.format(values[position].getCreatedAt());
		textView.setText(values[position].getName() + " - " + dateStr);
		// change the icon for Windows and iPhone
//		String s = values[position];
		/*if (s.startsWith("iPhone")) {
			imageView.setImageResource(R.drawable.no);
		} else {
			imageView.setImageResource(R.drawable.ok);
		}*/

		return rowView;
	}
	
}
