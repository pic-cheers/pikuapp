package com.piku.app;

import java.io.File;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.piku.cloud.common.model.Credentials;
import com.piku.tasks.LoginTask;
import com.piku.tasks.UploadPhotoTask;

public class CopyOfMainActivity extends Activity implements OnClickListener {

	private static final String DEFAULT = "DEFAULT";
	private PhotosObserver photoObserver = new PhotosObserver();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String username = preferences.getString("username", DEFAULT);
		if (!username.equals(DEFAULT)) {
			Intent intent = new Intent(this, PartiesActivity.class);
			intent.putExtra("username", username);
			startActivity(intent);
		}
		else {
			registerReceiver(new BroadcastReceiver() {
			    @Override
			    public void onReceive(Context context, Intent intent) {
			      Uri data = intent.getData();
			      Media media = readFromMediaStore(getApplicationContext(), data);
					SharedPreferences preferences = PreferenceManager
							.getDefaultSharedPreferences(getApplicationContext());
					long partyId = preferences.getLong("partyId", -1);
					if (partyId != -1) {
						new UploadPhotoTask(partyId).execute(media.file);
					}
			  }}, new IntentFilter(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE));
			
			// Photo Observer
			getApplicationContext().getContentResolver().registerContentObserver(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false,
					photoObserver);
			// On click handling
			Button btn1 = (Button) findViewById(R.id.loginbtn);
			Button btn2 = (Button) findViewById(R.id.registerbtn);
			btn1.setOnClickListener(this);
			btn2.setOnClickListener(this);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		this.getApplicationContext().getContentResolver()
				.unregisterContentObserver(photoObserver);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.loginbtn:
			EditText unEt = (EditText) findViewById(R.id.loginusername);
			EditText pwEt = (EditText) findViewById(R.id.loginpassword);
			String username = unEt.getText().toString();
			String password = pwEt.getText().toString();
			Credentials cred = new Credentials();
			cred.setUsername(username);
			cred.setPasswordHash(password);
			new LoginTask(getApplicationContext()).execute(cred);
			break;

		case R.id.registerbtn:
			Intent intent = new Intent(getApplicationContext(),
					RegistrationActivity.class);
			startActivity(intent);
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	private class PhotosObserver extends ContentObserver {

		public PhotosObserver() {
			super(null);
		}

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			Media media = readFromMediaStore(getApplicationContext(),
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			long partyId = preferences.getLong("partyId", -1);
			if (partyId != -1) {
				new UploadPhotoTask(partyId).execute(media.file

				);
			}
		}
	}

	private Media readFromMediaStore(Context context, Uri uri) {
		Cursor cursor = context.getContentResolver().query(uri, null, null,
				null, "date_added DESC");
		Media media = null;
		if (cursor.moveToNext()) {
			int dataColumn = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
			String filePath = cursor.getString(dataColumn);
			int mimeTypeColumn = cursor
					.getColumnIndexOrThrow(MediaColumns.MIME_TYPE);
			String mimeType = cursor.getString(mimeTypeColumn);
			media = new Media(new File(filePath), mimeType);
		}
		cursor.close();
		return media;
	}

	private class Media {
		private File file;
		@SuppressWarnings("unused")
		private String type;

		public Media(File file, String type) {
			this.file = file;
			this.type = type;
		}
	}

}
