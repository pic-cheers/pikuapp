package com.piku.tasks;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.piku.app.AppConfig;
import com.piku.app.PartiesActivity;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;
import com.piku.cloud.common.model.User;

public class RegistrationTask extends AsyncTask<Object, Void, Boolean> {

	private Context context;
	private String username;
	private File dpFile;

	public RegistrationTask(Context context) {
		this.context = context;
	}

	@Override
	protected Boolean doInBackground(Object... params) {
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
		User user = (User) params[0];
		dpFile = (File) params[1];
		boolean success = pikuClient.register(user);
		username = user.getUsername();
		return success;
	}
	
	@Override
	protected void onPostExecute(Boolean success) {
		if (!success) {
			Toast.makeText(context, "This username is already taken. Please try a different username.",
				Toast.LENGTH_SHORT).show();
		}
		else {
			if (dpFile != null) {
				new UploadFileTask(username).execute(dpFile);
			}
			Toast.makeText(context, "Registration was successful!", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(context, PartiesActivity.class);
			intent.putExtra("username", username);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		}
	}

}
