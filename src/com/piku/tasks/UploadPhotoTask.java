package com.piku.tasks;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.AsyncTask;
import android.util.Log;

import com.piku.app.AppConfig;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;

public class UploadPhotoTask extends AsyncTask<File, Void, Void> implements
		Callback<String> {

	private long partyId;

	public UploadPhotoTask(long partyId) {
		this.partyId = partyId;
	}

	@Override
	protected Void doInBackground(File... params) {
		Log.e("UPLOAD", "START");
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
		pikuClient.uploadPhoto(params[0], partyId, this);
		Log.e("UPLOAD", "COMPLETED");
		return null;
	}

	@Override
	public void success(String arg0, Response arg1) {
		Log.e("UPLOAD", "SUCCESS");
	}

	@Override
	public void failure(RetrofitError arg0) {
		Log.e("UPLOAD", "FAILURE");
	}

}
