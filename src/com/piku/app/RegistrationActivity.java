package com.piku.app;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.piku.cloud.common.model.User;
import com.piku.tasks.RegistrationTask;

public class RegistrationActivity extends Activity implements OnClickListener {

	private static final int SELECT_PHOTO = 100;
	
	private File dpFile;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		dpFile = null;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration_activity);
		 // On click handling
        Button btn = (Button) findViewById(R.id.signupbtn);
        btn.setOnClickListener(this);
        Button dpBtn = (Button) findViewById(R.id.adddp);
        dpBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(i, SELECT_PHOTO); 
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
	    super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
	    switch(requestCode) { 
	    	case SELECT_PHOTO:
	    		if(resultCode == RESULT_OK){  
	    			Uri selectedImage = imageReturnedIntent.getData();
	    			String[] proj = { MediaStore.Images.Media.DATA };
	    			Cursor cursor = getContentResolver().query(selectedImage, proj, null, null, null);
	    			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	    			cursor.moveToFirst();
	    			dpFile = new File(cursor.getString(column_index));
	    			ImageView imageView = (ImageView) findViewById(R.id.imageView1);
	    			imageView.setImageURI(null); 
	    			imageView.setImageURI(selectedImage);
	    		}
	    }
	}

	@Override
	public void onClick(View v) {
		EditText ruEt = (EditText) findViewById(R.id.regusername);
		EditText rnEt = (EditText) findViewById(R.id.regfullname);
		EditText rpEt = (EditText) findViewById(R.id.regpassword);
		String username = ruEt.getText().toString();
		String fullname = rnEt.getText().toString();
		String password = rpEt.getText().toString();
		User user = new User();
		user.setUsername(username);
		user.setName(fullname);
		user.setPasswordHash(password);
		new RegistrationTask(getApplicationContext()).execute(user, dpFile);
	}
	
}
