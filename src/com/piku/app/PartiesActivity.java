package com.piku.app;

import java.util.Arrays;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.piku.app.util.AppUtils;
import com.piku.cloud.common.model.Party;
import com.piku.tasks.CreatePartyTask;
import com.piku.tasks.LoadPartiesTask;

public class PartiesActivity extends Activity implements OnClickListener {

	private String username;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.parties_activity);
		
		AppUtils.resetDetails(getApplicationContext()); /** RESET PREVIOUS DETAILS **/
		
		Intent intent = getIntent();
		username = intent.getStringExtra("username");
		storeUsername();
		new LoadPartiesTask(this).execute(username);
		Button newPartyBtn = (Button) findViewById(R.id.newpartybtn);
		newPartyBtn.setOnClickListener(this);
		
		final Activity a = this;
		Button logoutBtn = (Button) findViewById(R.id.logoutbtn);
		logoutBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				AppUtils.resetDetails(getApplicationContext());
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
			    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			    startActivity(intent);
				AppUtils.exit(a);
			}
		});
	}

	private void storeUsername() {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("username", username); // value to store
		editor.commit();
	}

	@Override
	public void onClick(View v) {
		EditText partyNameEt = (EditText) findViewById(R.id.editText1);
		String newPartyName = partyNameEt.getText().toString();
		partyNameEt.setText("");
		Party party = new Party();
		party.setName(newPartyName);
		party.setOwnerUsername(username);
		party.setCreatedAt(new Date());
		party.setCollaborators(Arrays.asList(new String[] { username }));
		new CreatePartyTask(getApplicationContext()).execute(party);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		View rootView = findViewById(android.R.id.content);
		View loadingText = rootView.findViewById(R.id.textView2);
		loadingText.setVisibility(View.VISIBLE);
		new LoadPartiesTask(this).execute(username);
	}
	
}
