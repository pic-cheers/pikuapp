package com.piku.tasks;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.piku.app.AppConfig;
import com.piku.app.PartyActivity;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;
import com.piku.cloud.common.model.Photos;

public class PhotoFetchingTask extends AsyncTask<Void, Void, Void> {

	private static final ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
	
	private PartyActivity activity;

	public PhotoFetchingTask(PartyActivity activity) {
		this.activity = activity;
	}

	@Override
	protected Void doInBackground(Void... params) {
		final SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(activity.getApplicationContext());
		ses.scheduleWithFixedDelay(new Runnable() {
			@Override public void run() {
				long partyId = preferences.getLong("partyId", -1);
				if (partyId != -1) {
					Log.e("FETCHING", "NOW");
					PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
					Photos photos = pikuClient.fetchPhotos(partyId);
					List<String> urls = photos.getPhotoUrls();
					List<Bitmap> bitmaps = new ArrayList<Bitmap>();
					for (String url : urls) {
						try {
							InputStream in = new URL(AppConfig.API_BASE_URL + File.separator + url).openStream();
							BitmapFactory.Options options = new BitmapFactory.Options();
							options.inSampleSize = 8;
							Bitmap bitmap = BitmapFactory.decodeStream(in, null, options);
							bitmaps.add(bitmap);
						} catch (Exception e) {
							Log.e("Error", e.getMessage());
							e.printStackTrace();
						}
					}
					Log.e("FETCHED", bitmaps.size() + "");
					if (!bitmaps.isEmpty()) {
						activity.renderPhotos(bitmaps, urls);
					}
				}
			}
		}, 1, 5000, TimeUnit.MILLISECONDS);
		return null;
	}

}
