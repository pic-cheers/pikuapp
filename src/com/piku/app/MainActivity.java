package com.piku.app;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.piku.cloud.common.model.Credentials;
import com.piku.tasks.LoginTask;

public class MainActivity extends Activity implements OnClickListener {

	private static final String DEFAULT = "DEFAULT";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String username = preferences.getString("username", DEFAULT);
		if (!username.equals(DEFAULT)) {
			Intent intent = new Intent(this, PartiesActivity.class);
			intent.putExtra("username", username);
			startActivity(intent);
		} else {
			// On click handling
			Button btn1 = (Button) findViewById(R.id.loginbtn);
			Button btn2 = (Button) findViewById(R.id.registerbtn);
			btn1.setOnClickListener(this);
			btn2.setOnClickListener(this);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.loginbtn:
			EditText unEt = (EditText) findViewById(R.id.loginusername);
			EditText pwEt = (EditText) findViewById(R.id.loginpassword);
			String username = unEt.getText().toString();
			String password = pwEt.getText().toString();
			Credentials cred = new Credentials();
			cred.setUsername(username);
			cred.setPasswordHash(password);
			new LoginTask(getApplicationContext()).execute(cred);
			break;

		case R.id.registerbtn:
			Intent intent = new Intent(getApplicationContext(),
					RegistrationActivity.class);
			startActivity(intent);
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

}
