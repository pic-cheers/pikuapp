package com.piku.app;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.piku.cloud.common.model.Party;
import com.piku.tasks.AddCollaboratorTask;
import com.piku.tasks.DownloadPhotoTask;
import com.piku.tasks.PhotoFetchingTask;
import com.piku.tasks.ReloadPartyTask;

public class PartyActivity extends Activity implements
		CreateNdefMessageCallback, OnNdefPushCompleteCallback {
	
    private NfcAdapter mNfcAdapter;
    private static final int MESSAGE_SENT = 1;
	
	private boolean newParty;
	private Party party;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.party_activity);
		Intent intent = getIntent();
		if (intent.getAction() == null
				|| !intent.getAction().equals("android.nfc.action.NDEF_DISCOVERED")) {
			String partyStr = intent.getStringExtra("party");
			newParty = intent.getBooleanExtra("newParty", false);
			party = new Gson().fromJson(partyStr, Party.class);
			render();
		}
		new PhotoFetchingTask(this).execute();
		setupNFC();
	}

	public void render() {
		runOnUiThread(new Runnable() {
			@Override public void run() {
				SharedPreferences preferences = PreferenceManager
						.getDefaultSharedPreferences(getApplicationContext());
				SharedPreferences.Editor editor = preferences.edit();
				editor.putLong("partyId", party.getId());
				editor.commit();
				TextView partyName = (TextView) findViewById(R.id.partynametxt);
				SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
				String dateStr = sdf.format(party.getCreatedAt());
				partyName.setText(party.getName() + " - " + dateStr);
				TextView partyCollabs = (TextView) findViewById(R.id.partycollaboratorstxt);
				partyCollabs.setText(party.getCollaborators().toString());
			}
		});
	}

	public void renderPhotos(final List<Bitmap> bitmaps, final List<String> urls) {
		final Activity activity = this;
		runOnUiThread(new Runnable() {
			@Override public void run() {
				LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
				int cc = linearLayout.getChildCount();
				linearLayout.removeAllViews();
				int i = 0;
				List<String> toDown = new ArrayList<String>();
				for (Bitmap bitmap : bitmaps) {
					ImageView iv = new ImageView(activity);
					iv.setLayoutParams(new LayoutParams(150, 150));
					iv.setImageBitmap(bitmap);
					iv.setPadding(8, 8, 8, 8);
					linearLayout.addView(iv);
					if (i >= cc) {
						toDown.add(urls.get(i));
					}
					i ++;
				}
				new DownloadPhotoTask(party.getId(), toDown).execute();
			}
		});
	}

	private void setupNFC() {
		// Check for available NFC Adapter
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(this, "NFC is not available on this device", Toast.LENGTH_SHORT).show();
        } else {
            // Register callback to set NDEF message
            mNfcAdapter.setNdefPushMessageCallback(this, this);
            // Register callback to listen for message-sent success
            mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
        }
	}

	/**
     * Implementation for the CreateNdefMessageCallback interface
     */
    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        Time time = new Time();
        time.setToNow();
        String text = new Gson().toJson(party);
        String mimeType = "application/com.piku.app";
		NdefMessage msg = new NdefMessage(NdefRecord.createMime(mimeType, text.getBytes())
         /**
          * The Android Application Record (AAR) is commented out. When a device
          * receives a push with an AAR in it, the application specified in the AAR
          * is guaranteed to run. The AAR overrides the tag dispatch system.
          * You can add it back in to guarantee that this
          * activity starts when receiving a beamed message. For now, this code
          * uses the tag dispatch system.
          */
          //,NdefRecord.createApplicationRecord("com.example.android.beam")
        );
        return msg;
    }

    /**
     * Implementation for the OnNdefPushCompleteCallback interface
     */
    @Override
    public void onNdefPushComplete(NfcEvent arg0) {
        // A handler is needed to send messages to the activity when this
        // callback occurs, because it happens from a binder thread
        mHandler.obtainMessage(MESSAGE_SENT).sendToTarget();
    }
    
    /** This handler receives a message from onNdefPushComplete */
    private final PartyActivity act = this;
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_SENT:
				try {
					Thread.sleep(2500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				new ReloadPartyTask(act).execute(party.getId());
				Toast.makeText(getApplicationContext(),
					"Collaborator added to party " + party.getName() + "!",
						Toast.LENGTH_LONG).show();
				break;
			}
		}
	};

    @Override
    public void onResume() {
        super.onResume();
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        // onResume gets called after this to handle the intent
        setIntent(intent);
    }

    /**
     * Parses the NDEF Message from the intent and prints to the TextView
     */
    void processIntent(Intent intent) {
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        // record 0 contains the MIME type, record 1 is the AAR, if present
        String partyJson = new String(msg.getRecords()[0].getPayload());
        party = new Gson().fromJson(partyJson, Party.class);
        newParty = true;
        addCollaborator();
    }

	private void addCollaborator() {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		String username = preferences.getString("username", "DEFAULT");
		new AddCollaboratorTask(this).execute(party.getId(), username);
	}

	public void update(Party party) {
		this.party = party;
	}

}
