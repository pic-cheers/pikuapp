package com.piku.tasks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;

import android.os.AsyncTask;
import android.os.Environment;

import com.piku.app.AppConfig;

public class DownloadPhotoTask extends AsyncTask<Void, Void, Void> {

	private long partyId;
	private List<String> urls;

	public DownloadPhotoTask(long partyId, List<String> urls) {
		this.partyId = partyId;
		this.urls = urls;
	}

	@Override
	protected Void doInBackground(Void... params) {
		File storagePath = Environment.getExternalStorageDirectory();
		String localFileDir = storagePath.getAbsolutePath() + File.separator + "Download"
				+ File.separator + "PicCheers" + File.separator + partyId;
		File file = new File(localFileDir);
		file.mkdirs();
		for (String url : urls) {
			String localFilePath = storagePath.getAbsolutePath() + File.separator + "Download"
					+ File.separator + "PicCheers" + File.separator
					+ File.separator + url.replace("attachments/", "");
			File newFile = new File(localFilePath);
			if (!newFile.exists()) {
				try {
					newFile.setLastModified(System.currentTimeMillis());
					URL u = new URL(AppConfig.API_BASE_URL + File.separator + url);
					InputStream input = u.openStream();
					try {
						OutputStream output = new FileOutputStream(localFilePath);
						try {
							byte[] buffer = new byte[1500];
							int bytesRead = 0;
							while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
								output.write(buffer, 0, bytesRead);
							}
						} finally {
							output.close();
						}
					} finally {
						input.close();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
