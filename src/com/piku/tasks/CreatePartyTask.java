package com.piku.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.piku.app.AppConfig;
import com.piku.app.PartyActivity;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;
import com.piku.cloud.common.model.Party;

public class CreatePartyTask extends AsyncTask<Party, Void, Party> {

	private Context context;

	public CreatePartyTask(Context context) {
		this.context = context;
	}

	@Override
	protected Party doInBackground(Party... params) {
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
		Party party = pikuClient.create(params[0]);
		return party;
	}
	
	@Override
	protected void onPostExecute(Party result) {
		Intent intent = new Intent(context, PartyActivity.class);
		intent.putExtra("party", new Gson().toJson(result));
		intent.putExtra("newParty", true);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

}
