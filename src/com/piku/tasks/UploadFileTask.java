package com.piku.tasks;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.AsyncTask;
import android.util.Log;

import com.piku.app.AppConfig;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;

public class UploadFileTask extends AsyncTask<File, Void, Void> implements
		Callback<String> {

	private String username;

	public UploadFileTask(String username) {
		this.username = username;
	}

	@Override
	protected Void doInBackground(File... params) {
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
		pikuClient.uploadDP(params[0], username, this);
		return null;
	}

	@Override
	public void success(String arg0, Response arg1) {
		Log.e("UPLOAD", "SUCCESS");
	}

	@Override
	public void failure(RetrofitError arg0) {
		Log.e("UPLOAD", "FAILURE");
	}

}
