package com.piku.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.piku.app.AppConfig;
import com.piku.app.PartiesActivity;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;
import com.piku.cloud.common.model.Credentials;

public class LoginTask extends AsyncTask<Credentials, Void, Boolean> {

	private Context context;
	private String username;

	public LoginTask(Context context) {
		this.context = context;
	}

	@Override
	protected Boolean doInBackground(Credentials... params) {
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
		boolean authenticated = pikuClient.authenticate(params[0]);
		username = params[0].getUsername();
		return authenticated;
	}
	
	@Override
	protected void onPostExecute(Boolean success) {
		if (!success) {
			Toast.makeText(context, "Login failed", Toast.LENGTH_SHORT).show();
		}
		else {
			Intent intent = new Intent(context, PartiesActivity.class);
			intent.putExtra("username", username);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		}
	}

}
