package com.piku.tasks;

import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.piku.app.AppConfig;
import com.piku.app.PartiesListAdapter;
import com.piku.app.PartyActivity;
import com.piku.app.R;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;
import com.piku.cloud.common.model.Party;

public class LoadPartiesTask extends AsyncTask<String, Void, Set<Party>> {

	private Context context;

	public LoadPartiesTask(Context context) {
		this.context = context;
	}

	@Override
	protected Set<Party> doInBackground(String... params) {
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
		Set<Party> parties = pikuClient.loadParties(params[0]);
		return parties;
	}
	
	@Override
	protected void onPostExecute(Set<Party> result) {
		View rootView = ((Activity) context).findViewById(android.R.id.content);
		// Hide loading
		View loadingText = rootView.findViewById(R.id.textView2);
		loadingText.setVisibility(View.GONE);
		if (result.isEmpty()) {
			View noPartiesText = rootView.findViewById(R.id.TextView01);
			noPartiesText.setVisibility(View.VISIBLE);
		}
		else {
			View partiesText = rootView.findViewById(R.id.partynametxt);
			partiesText.setVisibility(View.VISIBLE);
			ListView partiesList = (ListView) rootView.findViewById(R.id.partieslist);
			partiesList.setVisibility(View.VISIBLE);
			ArrayAdapter<Party> adapter = new PartiesListAdapter(context, result.toArray(new Party[result.size()]));
			partiesList.setAdapter(adapter);
			partiesList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					Party party = (Party) parent.getItemAtPosition(position);
					Intent intent = new Intent(context, PartyActivity.class);
					intent.putExtra("party", new Gson().toJson(party));
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
				}
			});
		}
	}
	
}
