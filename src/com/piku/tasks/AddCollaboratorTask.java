package com.piku.tasks;

import android.os.AsyncTask;

import com.piku.app.AppConfig;
import com.piku.app.PartyActivity;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;

public class AddCollaboratorTask extends AsyncTask<Object, Void, Long> {

	private PartyActivity partyActivity;

	public AddCollaboratorTask(PartyActivity partyActivity) {
		this.partyActivity = partyActivity;
	}

	@Override
	protected Long doInBackground(Object... params) {
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(AppConfig.API_BASE_URL);
		long partyId = (long) params[0];
		pikuClient.addCollaborator(partyId, (String) params[1]);
		return partyId;
	}

	@Override
	protected void onPostExecute(Long partyId) {
		new ReloadPartyTask(partyActivity).execute(partyId);
	}

}
