package com.piku.tasks;

import android.os.AsyncTask;

import com.piku.app.AppConfig;
import com.piku.app.PartyActivity;
import com.piku.cloud.client.PikuCloudClient;
import com.piku.cloud.client.impl.RetrofitPikuCloudClient;
import com.piku.cloud.common.model.Party;

public class ReloadPartyTask extends AsyncTask<Long, Void, Void> {

	private PartyActivity partyActivity;

	public ReloadPartyTask(PartyActivity partyActivity) {
		this.partyActivity = partyActivity;
	}

	@Override
	protected Void doInBackground(Long... params) {
		PikuCloudClient pikuClient = new RetrofitPikuCloudClient(
				AppConfig.API_BASE_URL);
		Party party = pikuClient.loadParty(params[0]);
		partyActivity.update(party);
		partyActivity.render();
		return null;
	}

}
